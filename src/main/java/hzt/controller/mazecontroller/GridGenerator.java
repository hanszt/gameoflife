package hzt.controller.mazecontroller;

import hzt.model.StartPattern;
import hzt.model.grid.Pixel;
import javafx.geometry.Point2D;

import java.util.HashSet;
import java.util.Set;

public class GridGenerator {

    public Pixel[][] createRandomStartGrid(int width, int height) throws OutOfMemoryError {
        Pixel[][] grid = new Pixel[width][height];
        Set<Pixel> aliveSet = new HashSet<>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (Math.random() < 0.15) {
                    Pixel p = new Pixel(x, y, true);
                    grid[x][y] = p;
                    aliveSet.add(p);
                } else grid[x][y] = new Pixel(x, y, false);
            }
        }
        createExtraAliveNeighbours(aliveSet, grid);
        return grid;
    }

    private void createExtraAliveNeighbours(Set<Pixel> aliveSet, Pixel[][] grid) {
        for (Pixel p : aliveSet) {
            if (p.getX() - 1 >= 0) grid[p.getX() - 1][p.getY()].setAlive(true);
//            if (p.getY() - 1 >= 0) grid[p.getX()][p.getY() - 1].setAlive(true);
            if (p.getX() + 1 < grid.length /*&& p.getY() - 1 >= 0*/)
                grid[p.getX() + 1][p.getY() /*- 1*/].setAlive(true);
//            if (p.getY() + 1 < grid[0].length) grid[p.getX()][p.getY() + 1].setAlive(true);
        }
    }

    private static final double FRACTION = 2.2, MULTIPLIER = FRACTION - 1;

    public Pixel[][] createOrderedStartGrid(int width, int height) throws OutOfMemoryError {
        Pixel[][] grid = new Pixel[width][height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (/*x > width / FRACTION && y > height / FRACTION &&
                        x < width * MULTIPLIER / FRACTION && y < height * MULTIPLIER / FRACTION &&*/
                        ((y % 2 == 0 && x % 2 == 1) || (y % 2 == 1 && x % 2 == 0))) {
                    grid[x][y] = new Pixel(x, y, true);
                } else grid[x][y] = new Pixel(x, y, false);
            }
        }
        return grid;
    }

    public Pixel[][] createPatternedGrid(int width, int height, StartPattern pattern) {
        Pixel[][] grid = new Pixel[width][height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                grid[x][y] = new Pixel(x, y, false);
            }
        }
        if (pattern.isRandom()) {
            randomlyAddPatterns(grid, pattern.getPattern(), pattern.getAmount());
        } else addPatternsNextToEachOther(grid, pattern.getPattern());
        return grid;
    }

    private static final int SPACING = 4;

    private void addPatternsNextToEachOther(Pixel[][] grid, boolean[][] pattern) {
        Point2D upperLeft = Point2D.ZERO;
        while (upperLeft.getY() + pattern[0].length < grid[0].length) {
            if (upperLeft.getX() + pattern.length < grid.length) {
                addPattern(grid, pattern, upperLeft);
                upperLeft = upperLeft.add(new Point2D(pattern.length + SPACING, 0));
            } else upperLeft = new Point2D(0, upperLeft.getY() + pattern[0].length + SPACING);
        }
    }

    private void randomlyAddPatterns(Pixel[][] grid, boolean[][] pattern, int amount) {
        for (int i = 0; i < amount; i++) {
            int startX = (int) (Math.random() * (grid.length - pattern.length));
            int startY = (int) (Math.random() * (grid[0].length - pattern[0].length));
            addPattern(grid, pattern, new Point2D(startX, startY));
        }
    }

    private void addPattern(Pixel[][] grid, boolean[][] pattern, Point2D upperLeftPoint) {
        int startX = (int) upperLeftPoint.getX(), startY = (int) upperLeftPoint.getY();
        for (int y = startY; y < startY + pattern[0].length; y++) {
            for (int x = startX; x < startX + pattern.length; x++) {
                grid[x][y].setAlive(pattern[x - startX][y - startY]);
            }
        }
    }

    private boolean[][] rotate90DegCounterClockWise(boolean[][] pattern, int times) {
        for (int i = 0; i < times; i++) pattern = rotate90DegCounterClockWise(pattern);
        return pattern;
    }

    boolean[][] rotate90DegCounterClockWise(boolean[][] input) {
        int n = input.length;
        int m = input[0].length;
        boolean[][] output = new boolean[m][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                output[j][n - 1 - i] = input[i][j];
            }
        }
        return output;
    }
}
