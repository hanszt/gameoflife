package hzt.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import static hzt.controller.AppConstants.*;
import static javafx.util.Duration.seconds;

public abstract class AppVars {

    final Stage primaryStage;
    final Scene scene;
    final StackPane root;
    final Canvas bgCanvas, canvas;
    final Timeline timeline;

    AppVars(Stage stage) {
        this.primaryStage = stage;
        this.root = new StackPane();
        this.scene = setupScene();
        this.canvas = setupCanvas();
        this.bgCanvas = setupCanvas();
        this.timeline = new Timeline(new KeyFrame(seconds(1. / INIT_FRAMERATE), event -> manageSim(bgCanvas)));
    }

    abstract void manageSim(Canvas mazeCanvas);

    public AppManager.RootBcUpdater rootBcUpdater;

    private Scene setupScene() {
        rootBcUpdater = (c) -> root.setBackground(new Background(new BackgroundFill(c, CornerRadii.EMPTY, Insets.EMPTY)));
        Scene scene = new Scene(root, INIT_SCENE_SIZE.width, INIT_SCENE_SIZE.height);
        root.setAlignment(Pos.CENTER);
        return scene;
    }

    public static void setCanvasSize(Canvas canvas, int mazeWidth, int mazeHeight) {
        if (mazeWidth > mazeHeight) {
            canvas.setWidth(CANVAS_LONG_SIDE);
            canvas.setHeight(CANVAS_LONG_SIDE * mazeHeight / (double) mazeWidth);
        } else {
            canvas.setWidth(CANVAS_LONG_SIDE * mazeWidth / (double) mazeHeight);
            canvas.setHeight(CANVAS_LONG_SIDE);
        }
    }

    private Canvas setupCanvas() {
        Canvas canvas = new Canvas(INIT_CANVAS_SIZE.width, INIT_CANVAS_SIZE.height);
        canvas.setScaleX(scene.getWidth() / canvas.getWidth());
        canvas.setScaleY(scene.getHeight() / canvas.getHeight());
        return canvas;
    }

    int genSolveCounter;
    public int frameCount = 0;
    public double stopTimeSim;

    double runTime;
    double startTimeSim;
    double tStart;
    double tStop;

    public Scene getScene() {
        return scene;
    }

    public Timeline getTimeline() {
        return timeline;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public Canvas getBgCanvas() {
        return bgCanvas;
    }
}
