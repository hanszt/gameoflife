package hzt.controller;

import hzt.model.grid.Pixel;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

import java.awt.*;

import static java.lang.Math.sqrt;

public class AppConstants {

    // constants
    private static final Rectangle2D PRIMARY_SCREEN_BOUNDS = Screen.getPrimary().getVisualBounds();
    public static final int MENU_WIDTH = 250;
    private static final double INIT_SCENE_HEIGHT = PRIMARY_SCREEN_BOUNDS.getHeight();
    // don't make the canvas to large or an exception will occur
    public static final int CANVAS_LONG_SIDE = 3200;
    private static final double GOLDEN_RATIO = (1 + sqrt(5)) / 2;

    public static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    public static final Dimension INIT_SCENE_SIZE = new Dimension((int) (INIT_SCENE_HEIGHT / GOLDEN_RATIO), (int) INIT_SCENE_HEIGHT);
    public static final Dimension INIT_CANVAS_SIZE = new Dimension((int) (CANVAS_LONG_SIDE / GOLDEN_RATIO), CANVAS_LONG_SIDE);

    static final int ANNOUNCEMENT_FONT_SIZE = 250;

    public static final double INIT_FRAMERATE = 1; //f/s
    public static final double MIN_SCALE_FACTOR = 0.05, MAX_SCALE_FACTOR = 10;

    public static final String DOTTED_LINE = "----------------------------------------------------------------------------------------\n";
    static final String TITLE = "Cellular Automaton";
    public static final String GAME_OF_LIFE = "Game of life";
    static final String CLOSING_MESSAGE = "See you next time!";
    public static final String OTHER_1 = "Other1";
    public static final String OTHER_2 = "Other2";

    // http://patorjk.com/software/taag/#p=display&f=Big&t=A%20%20'Maze'%20ng

    public static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_BRIGHT_PURPLE = "\u001B[95m";
    private static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BRIGHT_RED = "\u001B[91m";

//            ANSI_PURPLE = "\u001B[35m", ANSI_CYAN = "\u001B[36m", ANSI_WHITE = "\u001B[37m",
//            ANSI_BRIGHT_BLACK = "\u001B[90m", ANSI_BRIGHT_RED = "\u001B[91m", ANSI_BRIGHT_GREEN = "\u001B[92m",
//            ANSI_BRIGHT_YELLOW = "\u001B[93m", ANSI_BRIGHT_BLUE = "\u001B[94m", ANSI_BRIGHT_PURPLE = "\u001B[95m",
//            ANSI_BRIGHT_CYAN = "\u001B[96m", ANSI_BRIGHT_WHITE = "\u001B[97m", ANSI_BLACK_BACKGROUND = "\u001B[40m",
//            ANSI_RED_BACKGROUND = "\u001B[41m", ANSI_GREEN_BACKGROUND = "\u001B[42m", ANSI_BLACK = "\u001B[30m",
//            ANSI_BLUE_BACKGROUND = "\u001B[44m", ANSI_PURPLE_BACKGROUND = "\u001B[45m", ANSI_CYAN_BACKGROUND = "\u001B[46m",
//            ANSI_WHITE_BACKGROUND = "\u001B[47m", ANSI_BRIGHT_BG_BLACK = "\u001B[100m", ANSI_BRIGHT_BG_RED = "\u001B[101m",
//            ANSI_BRIGHT_BG_GREEN = "\u001B[102m", ANSI_BRIGHT_BG_YELLOW = "\u001B[103m", ANSI_BRIGHT_BG_BLUE = "\u001B[104m",
//            ANSI_BRIGHT_BG_PURPLE = "\u001B[105m", ANSI_BRIGHT_BG_CYAN = "\u001B[106m", ANSI_BRIGHT_BG_WHITE = "\u001B[107m",

    public static final String TITLE_FX = ANSI_BRIGHT_PURPLE + "Cellular Automaton" + ANSI_RESET;

    public static Pixel[][] createCopyOfMazePixelGrid(Pixel[][] pixels) {
        int width = pixels.length, height = pixels[0].length;
        Pixel[][] copyOfPixels = new Pixel[width][height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                copyOfPixels[x][y] = pixels[x][y];
            }
        }
        return copyOfPixels;
    }
}