package hzt.controller;

import hzt.controller.mazecontroller.GridGenerator;
import hzt.model.grid.Pixel;
import hzt.view.*;
import javafx.animation.Timeline;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.Objects;

import static hzt.controller.AppConstants.*;
import static java.lang.String.format;
import static java.lang.System.*;
import static javafx.animation.Animation.Status.PAUSED;
import static javafx.scene.paint.Color.YELLOW;

public class AppManager extends AppVars {

    private static int instances = 0;
    private final int instance = ++instances;

    private final ColorController c = new ColorController();
    private final Animate ag = new Animate(this, new GridGenerator(), c);
    private final StatsController s = new StatsController(this, ag);
    private final UserInputController uc = new UserInputController(this, ag, c);

    public AppManager(Stage primaryStage) {
        super(primaryStage);
        runTime = nanoTime();
        GridPane controlsGrid = uc.setupControls(s);
        rootBcUpdater.update(c.getBackGroundColor());
        // hPos, vPos, hSpan, vSpan
        root.getChildren().addAll(canvas, bgCanvas, controlsGrid);
        primaryStage.setScene(scene);
        timeline.setCycleCount(Timeline.INDEFINITE);
        bindBackgroundCanvasToDrawCanvas(canvas, bgCanvas);
        primaryStage.setTitle(format("%s (%d)", TITLE, instance));
        primaryStage.setMinWidth(400);
        primaryStage.setMinHeight(400);
        primaryStage.setOnCloseRequest(e -> printClosingText());
        start();
        addResponsiveSceneMovement(scene, bgCanvas);
        primaryStage.show();
    }

    public interface RootBcUpdater {
        void update(Color bcColor);
    }

    private void bindBackgroundCanvasToDrawCanvas(Canvas canvas, Canvas bgCanvas) {
        canvas.widthProperty().bind(bgCanvas.widthProperty());
        canvas.heightProperty().bind(bgCanvas.heightProperty());
        canvas.scaleXProperty().bind(bgCanvas.scaleXProperty());
        canvas.scaleYProperty().bind(bgCanvas.scaleYProperty());
        canvas.translateXProperty().bind(bgCanvas.translateXProperty());
        canvas.translateYProperty().bind(bgCanvas.translateYProperty());
        canvas.rotateProperty().bind(bgCanvas.rotateProperty());
    }

    public SliderUpdater zoomSliderUpdater;

    private Point2D initPoint;

    private void addResponsiveSceneMovement(Scene scene, Node node) {
        scene.setOnMousePressed(e -> initPoint = new Point2D(node.getTranslateX() - e.getX(), node.getTranslateY() - e.getY()));
        scene.setOnMouseDragged(e -> {
            Point2D newOfSet = new Point2D(e.getX() + initPoint.getX(), e.getY() + initPoint.getY());
            //first update the sliders, and then the actual parameters in sim. Then there will not be an interaction!
            uc.sliderUpdater.updateSliders(-newOfSet.getX(), newOfSet.getY(), bgCanvas.getRotate());
            node.setTranslateX(newOfSet.getX());
            node.setTranslateY(newOfSet.getY());
        });
        addMouseScrolling(node);
    }

    private void addMouseScrolling(Node node) {
        Parent root = scene.getRoot();
        root.setOnScroll(e -> {
            // Adjust the zoom factor as per your requirement
            double zoomFactor = 1;
            if (node.getScaleX() > MIN_SCALE_FACTOR && e.getDeltaY() < 0) zoomFactor = 0.9;
            else if (node.getScaleX() < MAX_SCALE_FACTOR && e.getDeltaY() > 0) zoomFactor = 1.1;
            zoomSliderUpdater.updateSliders(node.getScaleX() * zoomFactor, 0, 0);
        });
    }

    public void start() {
        out.printf("Simulation of instance %d starting...\n", instance);
        startTimeSim = nanoTime();
        setCanvasSize(bgCanvas, ag.getGridSize().width, ag.getGridSize().height);
        ag.initializeAnimationVars(bgCanvas);
        drawStartScreen(canvas, bgCanvas);
        initializeTimeLine();
    }

    public void updateFrameIfPaused() {
        if (Objects.equals(getTimeline().getStatus(), PAUSED)) manageSim(bgCanvas);
    }

    private void initializeTimeLine() {
        tStart = nanoTime();
        timeline.play();
    }

    private void drawStartScreen(Canvas wallsCanvas, Canvas mazeCanvas) {
        ag.drawBackground(wallsCanvas, 0);
        GraphicsContext gc = mazeCanvas.getGraphicsContext2D();
        gc.clearRect(0, 0, mazeCanvas.getWidth(), mazeCanvas.getHeight());
        gc.setStroke(YELLOW);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setLineWidth(10);
        gc.setFont(new Font("", ANNOUNCEMENT_FONT_SIZE));
        gc.strokeText(TITLE /*+ "Click to Start"*/, mazeCanvas.getWidth() / 2, mazeCanvas.getHeight() / 2);
        out.println("\n\n" + DOTTED_LINE + "\n" + TITLE_FX);
        out.println("This program shows the working s of the game of life. \n\n" +
                "See how the rules play out... ಠᴗಠ\nEnjoy!\n" + DOTTED_LINE);
    }

    public void manageSim(Canvas mazeCanvas) {
        try {
            if (nanoTime() - tStart > 2e9) ag.manageGameOfLife(s, mazeCanvas);
        } catch (OutOfMemoryError e) {
            err.println("The process takes up to much memory.\n" +
                    "The program will close now.");
            closeStage();
        }
    }

    public void followDrawSpot(Pixel currentDrawSpot) {
        try {
            double translationX = (ag.getGridSize().width / 2. - currentDrawSpot.getX()) * ag.getUnitSize() * bgCanvas.getScaleX();
            double translationY = (ag.getGridSize().height / 2. - currentDrawSpot.getY()) * ag.getUnitSize() * bgCanvas.getScaleY();
            uc.sliderUpdater.updateSliders(-translationX, translationY, 0);
        } catch (NullPointerException e) {
            err.println(e.getMessage());
        }
    }

    public void restartSim() {
        printRestartingText();
        timeline.stop();
        resetForRestart();
        start();
    }

    private void printRestartingText() {
        out.printf("restarting animation...\nAnimation Runtime: %.2f seconds\n%s\n",
                (nanoTime() - startTimeSim) / 1e9, DOTTED_LINE);
    }

    private void printClosingText() {
        out.printf("%s\nAnimation Runtime of instance %d: %.2f seconds\n%s\n", CLOSING_MESSAGE,
                instance, (nanoTime() - runTime) / 1e9, DOTTED_LINE);
    }

    private void resetForRestart() {
        uc.sliderUpdater.updateSliders(0, 0, 0);
        reset();
    }

    public void reset() {
        tStart = tStop = startTimeSim = 0;
        genSolveCounter = 0;
        s.setInitStats(true);
    }

    public void stopSim() {
        timeline.stop();
    }

    private void closeStage() {
        timeline.stop();
        primaryStage.close();
    }

    public static int getInstances() {
        return instances;
    }
}
