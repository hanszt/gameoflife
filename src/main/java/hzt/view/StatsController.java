package hzt.view;

import hzt.controller.AppManager;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import static hzt.controller.AppConstants.INIT_FRAMERATE;
import static hzt.controller.AppConstants.MENU_WIDTH;
import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.paint.Color.YELLOW;

public class StatsController {

    private static final int STAT_FONT_SIZE = 10;
    private static final int NUMBER_POS = 170;

    private final AppManager m;
    private final Animate ag;

    public StatsController(AppManager m, Animate ag) {
        this.m = m;
        this.ag = ag;
    }

    private GraphicsContext graphics;
    private boolean initStats = true; // this is needed to set the initial conditions

    Canvas setupStatsCanvas(@SuppressWarnings("SameParameterValue") int sideLength) {
        Canvas statsCanvas = new Canvas(sideLength, sideLength);
        graphics = statsCanvas.getGraphicsContext2D();
        return statsCanvas;
    }

    void getGenStats() {
        if (initStats) {
            initStats = false;
            graphics.setTextAlign(TextAlignment.LEFT);
            graphics.setFont(Font.font("", STAT_FONT_SIZE));
            graphics.setFill(BLACK);
            graphics.fillRect(0, 0, MENU_WIDTH, m.getScene().getHeight());
        }
        fillStatsWithBlackBox(graphics);
        graphics.setFill(YELLOW);
        getStatNames(graphics);
        graphics.fillText(String.format("\n%d\n%d\n%d\n%d\n%.0f f/s\n",
                ag.gridSize.width, ag.gridSize.height, ag.aliveCells, ag.gridSize.width * ag.gridSize.height - ag.aliveCells,
                (int) m.getTimeline().getRate() * INIT_FRAMERATE), NUMBER_POS, STAT_FONT_SIZE);
    }

    private void getStatNames(GraphicsContext graphics) {
        graphics.fillText("Stats:\nGrid width: \nGrid height: \n" +
                "Alive cells: \nDead cells: \nFramerate:\n", 5, STAT_FONT_SIZE);
    }

    private void fillStatsWithBlackBox(GraphicsContext graphics) {
        graphics.setFill(BLACK);
        graphics.fillRect(0, 0, MENU_WIDTH, m.getScene().getHeight() / 4);
    }

    public void setInitStats(boolean initStats) {
        this.initStats = initStats;
    }
}
