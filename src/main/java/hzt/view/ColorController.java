package hzt.view;

import javafx.scene.paint.Color;

import static javafx.scene.paint.Color.*;

public class ColorController {

    private static final int MAX_RGB = 1;

    Color onColor;
    Color bottomOnColor;
    Color offColor;
    Color bottomoffColor;
    Color nodeColor;
    Color beginSolutionColor;
    Color endSolutionColor;
    Color backGroundColor;

    public ColorController() {
        this.offColor = DARKBLUE;
        this.bottomoffColor = LIGHTBLUE;
        this.onColor = ORANGE;
        this.bottomOnColor = YELLOW;
        this.backGroundColor = SANDYBROWN;
        this.nodeColor = BLACK;
        this.beginSolutionColor = rgb(0, 255, 0);
        this.endSolutionColor = rgb(255, 0, 0);
    }

    public Color getColorByHeight(Color color1, Color color2, int currentPos, int length) {
        double r, g, b;
        r = (MAX_RGB * (color1.getRed() + ((-Math.cos(Math.PI * currentPos / length) + 1) / 2) * (color2.getRed() - color1.getRed())));
        g = (MAX_RGB * (color1.getGreen() + ((-Math.cos(Math.PI * currentPos / length) + 1) / 2) * (color2.getGreen() - color1.getGreen())));
        b = (MAX_RGB * (color1.getBlue() + ((-Math.cos(Math.PI * currentPos / length) + 1) / 2) * (color2.getBlue() - color1.getBlue())));
        return new Color(r, g, b, 1);
    }


    public interface ColorUpdater {
        void updateColorPickers();
    }

    ColorUpdater colorUpdater;

    void resetColors() {
        this.offColor = DARKBLUE;
        this.bottomoffColor = LIGHTBLUE;
        this.onColor = ORANGE;
        this.bottomOnColor = YELLOW;
        this.backGroundColor = SANDYBROWN;
        this.nodeColor = BLACK;
        beginSolutionColor = rgb(0, 255, 0);
        endSolutionColor = rgb(255, 0, 0);
        colorUpdater.updateColorPickers();
    }

    public Color getBackGroundColor() {
        return backGroundColor;
    }

    public Color getOnColor() {
        return onColor;
    }

    public Color getBPathColor() {
        return bottomOnColor;
    }
}
