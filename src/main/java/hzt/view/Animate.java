package hzt.view;

import hzt.controller.AppManager;
import hzt.controller.mazecontroller.GridGenerator;
import hzt.model.grid.Pixel;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.System.nanoTime;
import static java.lang.System.out;

public class Animate extends AnimationVars {

    public Animate(AppManager m, GridGenerator mg, ColorController c) {
        super(m, mg, c);
    }

    public void initializeAnimationVars(Canvas canvas) {
        runs++;
        resetGenVars();
        if(startGrid.equals(PATTERNED)) grid = mg.createPatternedGrid(gridSize.width, gridSize.height, startPattern);
        else if (startGrid.equals(ORDERED)) grid = mg.createOrderedStartGrid(gridSize.width, gridSize.height);
        else grid = mg.createRandomStartGrid(gridSize.width, gridSize.height);
        unitSize = canvas.getHeight() / gridSize.height;
    }

    public void manageGameOfLife(StatsController s, Canvas drawCanvas) {
        frameCount++;
        if (startTime == 0) initiate(drawCanvas);
        runGameOfLife(drawCanvas.getGraphicsContext2D());
        s.getGenStats();
    }

    private void initiate(Canvas canvas) {
        startTime = nanoTime();
        GraphicsContext gc = canvas.getGraphicsContext2D();
        out.println("Animation starting...");
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

        out.println("Animation started...");
    }

    public void drawBackground(Canvas bgCanvas, double angle) {
        double v0 = sin(angle), v1 = cos(angle);
        lg = new LinearGradient(v0, v1, 0, 0, true, CycleMethod.REFLECT,
                new Stop(0, c.offColor), new Stop(1, c.bottomoffColor));
        GraphicsContext gc = bgCanvas.getGraphicsContext2D();
        gc.setFill(lg);
        gc.fillRect(0, 0, bgCanvas.getWidth(), bgCanvas.getHeight());
    }

    private void runGameOfLife(GraphicsContext graphics) {
        aliveCells = 0;
        for (Pixel[] pixels : grid) {
            for (Pixel current : pixels) {
                if (current.isAlive()) {
                    aliveCells++;
                    graphics.setFill(c.getColorByHeight(c.onColor, c.bottomOnColor, current.getY(), gridSize.height));
                } else {
                    graphics.setFill(c.getColorByHeight(c.offColor, c.bottomoffColor, current.getY(), gridSize.height));
                }
                graphics.fillRect(current.getX() * unitSize, current.getY() * unitSize, unitSize, unitSize);
            }
        }
        updateGridBasedOnGameOfLifeRules();
    }

    private void updateGridBasedOnGameOfLifeRules() {
        boolean[][] isAliveGrid = getBooleanMatrix(grid);
        for (int x = 0; x < grid.length; x++) {
            for (int y = 0; y < grid[x].length; y++) {
                int aliveNeighbours = getAliveNeighbourCount(isAliveGrid, x, y);
                if (isAliveGrid[x][y] && (aliveNeighbours < 2 || aliveNeighbours > 3)) grid[x][y].setAlive(false);
                if (!isAliveGrid[x][y] && aliveNeighbours == 3) grid[x][y].setAlive(true);
            }
        }
    }

    private boolean[][] getBooleanMatrix(Pixel[][] grid) {
        boolean[][] matrix = new boolean[grid.length][grid[0].length];
        for (int x = 0; x < grid.length; x++) {
            for (int y = 0; y < grid[0].length; y++) {
                matrix[x][y] = grid[x][y].isAlive();
            }
        }
        return matrix;
    }

    private int getAliveNeighbourCount(boolean[][] alive, int thisX, int thisY) {
        int aliveNeighbourCount = 0;
        int xSize = alive.length, xStart = mod(thisX - 1, xSize), ySize = alive[0].length, yStart = mod(thisY - 1, ySize);
        for (int y = yStart, i = 0; i < 3; y = mod(y + 1, ySize), i++) {
            for (int x = xStart, j = 0; j < 3; x = mod(x + 1, xSize), j++) {
                if (!(x == thisX && y == thisY) && alive[x][y]) aliveNeighbourCount++;
            }
        }
        return aliveNeighbourCount;
    }

    private int mod(int a, int modulo) {
        int aMod = a % modulo;
        if (aMod < 0) aMod = a + modulo;
        return aMod;
    }

    private void resetGenVars() {
        startTime = stopTime = 0;
    }
}
