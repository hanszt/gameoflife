package hzt.view;

import hzt.controller.AppManager;
import hzt.controller.mazecontroller.GridGenerator;
import hzt.model.StartPattern;
import hzt.model.grid.Pixel;
import javafx.scene.paint.LinearGradient;

import java.awt.*;

abstract class AnimationVars {

    final AppManager m;
    final GridGenerator mg;
    final ColorController c;

    AnimationVars(AppManager m, GridGenerator mg, ColorController c) {
        this.m = m;
        this.mg = mg;
        this.c = c;
    }

    // animation constants
    private static final Dimension INIT_GRID_SIZE = new Dimension(65, 65); // nog naar kijken

    int runs;
    int aliveCells;
    int frameCount = 0;

    double unitSize;
    double imperfectPercentage;
    double startTime;
    double stopTime;

    LinearGradient lg;
    Dimension gridSize = INIT_GRID_SIZE;
    Pixel[][] grid;
    StartPattern startPattern = StartPattern.GLIDER;

   public static final String ORDERED = "ordered", RANDOM = "random", PATTERNED = "patterned";

    String startGrid = PATTERNED;

    public double getUnitSize() {
        return unitSize;
    }

    public Dimension getGridSize() {
        return gridSize;
    }
}
