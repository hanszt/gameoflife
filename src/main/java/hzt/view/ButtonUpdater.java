package hzt.view;

public interface ButtonUpdater {

    void update(boolean condition);
}
