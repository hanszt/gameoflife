package hzt.view;

import hzt.controller.AppManager;
import hzt.model.StartPattern;
import hzt.model.custom_controls.SwitchButton;
import hzt.model.data.AutomatonEnum;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import static hzt.controller.AppConstants.*;
import static hzt.controller.AppManager.getInstances;
import static hzt.controller.AppVars.setCanvasSize;
import static java.lang.Math.acos;
import static javafx.animation.Animation.Status.RUNNING;

public class UserInputController {

    private static final int DIST_FROM_EDGE = 20;
    private static final int PREF_WIDTH_MENU = 200;
    private static final double OPACITY_VALUE = 0.7;

    private final AppManager m;
    private final Animate a;
    private final ColorController c;
    private final GridPane controlsGrid;

    public UserInputController(AppManager m, Animate a, ColorController c) {
        this.m = m;
        this.a = a;
        this.c = c;
        this.controlsGrid = setupControlsGridPane();
    }

    private int vPosControls = 0;
    private Canvas statsCanvas;
    private double controlsOpacity = OPACITY_VALUE;

    public GridPane setupControls(StatsController s) {
        statsCanvas = s.setupStatsCanvas(PREF_WIDTH_MENU);
        statsCanvas.setVisible(false);
        controlsGrid.add(statsCanvas, 0, 0, 2, 1);
        setupLabels();
        setupDropdownMenus(controlsGrid);
        setupButtons(controlsGrid);
        setupColorPickers(controlsGrid);
        setupSliders(controlsGrid);
        vPosControls = 0;
        for (Node n : controlsGrid.getChildren()) setGlobalProperties(n);
        return controlsGrid;
    }

    private void setGlobalProperties(Node n) {
        n.setOpacity(controlsOpacity);
        n.setCursor(Cursor.HAND);
    }

    private void setupLabels() {
        setupLabel(controlsGrid, INIT_SCENE_SIZE.height, vPosControls++);
    }

    private void setupLabel(GridPane controlsGrid, int prefHeight, int vPos) {
        Label label = new Label();
        label.setTextFill(c.backGroundColor);
        label.setPrefHeight(UserInputController.PREF_WIDTH_MENU);
        label.setPrefHeight(prefHeight);
        controlsGrid.add(label, 0, vPos);
    }

    private GridPane setupControlsGridPane() {
        GridPane grid = new GridPane();
        grid.setHgap(2);
        grid.setVgap(4);
        grid.setAlignment(Pos.CENTER);
//        grid.setPrefWidth(AppConstants.SCREEN_SIZE.width);
//        grid.setPrefHeight(AppConstants.SCREEN_SIZE.height);
        double yScaleFactor = m.getScene().getHeight() / INIT_SCENE_SIZE.height;
        if (yScaleFactor > 1) yScaleFactor = 1;
        grid.setScaleY(yScaleFactor);
        grid.setPadding(new Insets(DIST_FROM_EDGE, DIST_FROM_EDGE, DIST_FROM_EDGE * 2, DIST_FROM_EDGE));
        return grid;
    }

    private void setupDropdownMenus(GridPane grid) {
        setupDropdownMenuStartGrid(grid);
        setupDropdownMenuPatterns(grid);
        setupDropdownMenuGridWidth(grid);
        setupDropdownMenuGridHeight(grid);
    }

    private static final int MAX_NUMBER_OF_INSTANCES = 4;

    SwitchButton pauseButton;

    private void setupButtons(GridPane grid) {
        Button restart = setupButton(grid, "restart", 0, vPosControls);
        pauseButton = setupSwitchButton(grid, false, "play sim", "pause sim", 1, vPosControls++);
        Button resetColors = setupButton(grid, "resetColors", 0, vPosControls);
        Button opacityButton = setupButton(grid, getOpacityButtonLabel(), 1, vPosControls++);
        Button recenterButton = setupButton(grid, "Recenter", 0, vPosControls);
        Button newInstanceButton = setupButton(grid, getNewInstanceButtonLabel(), 1, vPosControls++);
        Button showStatsButton = setupButton(grid, getStatsModeLabel(), 0, vPosControls++);
        pauseButton.setActiveStyle("-fx-background-color: lightgreen;" +
                "-fx-text-fill: darkred"/* +
                        "-fx-border-color: darkred"*/);
        pauseButton.setInActiveStyle(null);
        restart.setOnAction(e -> m.restartSim());
        pauseButton.getMultipleEventsHandler().addEventHandler(e -> {
            if (m.getTimeline().getStatus().equals(RUNNING)) m.getTimeline().pause();
            else m.getTimeline().play();
        });
        opacityButton.setOnAction(e -> {
            controlsOpacity = controlsOpacity == OPACITY_VALUE ? 1 : OPACITY_VALUE;
            for (Node c : controlsGrid.getChildren()) c.setOpacity(controlsOpacity);
            opacityButton.setText(getOpacityButtonLabel());
            m.updateFrameIfPaused();
        });
        showStatsButton.setOnAction(e -> {
            statsCanvas.setVisible(!statsCanvas.isVisible());
            showStatsButton.setText(getStatsModeLabel());
            m.updateFrameIfPaused();
        });
        newInstanceButton.setOnAction(e -> {
            if (getInstances() < MAX_NUMBER_OF_INSTANCES) new AppManager(new Stage());
            else {
                newInstanceButton.setDisable(true);
                newInstanceButton.setText(getNewInstanceButtonLabel());
            }
        });
        recenterButton.setOnAction(e -> {
            Canvas canvas = m.getBgCanvas();
            canvas.setRotate(0);
            canvas.setTranslateX(0);
            canvas.setTranslateY(0);
            sliderUpdater.updateSliders(0, 0, 0);
        });
        resetColors.setOnAction(e -> {
            c.resetColors();
            a.drawBackground(m.getCanvas(), 0);
        });
        newInstanceButton.setOnAction(e -> {
            if (getInstances() < MAX_NUMBER_OF_INSTANCES) new AppManager(new Stage());
            else {
                newInstanceButton.setDisable(true);
                newInstanceButton.setText(getNewInstanceButtonLabel());
            }
        });
    }

    private String getStatsModeLabel() {
        return statsCanvas.isVisible() ? "hide stats" : "show stats";
    }

    private String getNewInstanceButtonLabel() {
        return getInstances() < MAX_NUMBER_OF_INSTANCES ? "start new instance" : "Maximum reached";
    }

    private String getOpacityButtonLabel() {
        return controlsOpacity == OPACITY_VALUE ? "solid" : "transparent";
    }

    private Button setupButton(GridPane grid, String text, int hPos, int vPos) {
        Button button = new Button(text);
        button.setPrefWidth(PREF_WIDTH_MENU / 2.);
        button.setCursor(Cursor.HAND);
        grid.add(button, hPos, vPos, 1, 1);
        return button;
    }

    private SwitchButton setupSwitchButton(GridPane grid, boolean on, String onText, String offText, int hPos, int vPos) {
        SwitchButton switchButton = new SwitchButton(on, onText, offText);
        switchButton.setPrefWidth(PREF_WIDTH_MENU / 2.);
        switchButton.setMinWidth(PREF_WIDTH_MENU / 2.);
        switchButton.getMultipleEventsHandler().addEventHandler(e -> m.updateFrameIfPaused());
        grid.add(switchButton, hPos, vPos);
        return switchButton;
    }

    @SuppressWarnings("all")
    private List<GridDimension> getDimensionList(String label, int min, int interval, int size) {
        List<GridDimension> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            int length = (int) (min + i * interval);
            list.add(new GridDimension(label, makeOdd(length)));
        }
        return list;
    }

    private int makeOdd(int d) {
        if (d % 2 == 0) d++;
        if (d < 3) d = 3;
        return d;
    }

    private void setupDropdownMenuStartGrid(GridPane grid) {
        final ComboBox<String> menu = new ComboBox<>();
        setComboBox(menu, grid, vPosControls++);
        List<String> list = new ArrayList<>(Arrays.asList(AnimationVars.RANDOM, AnimationVars.PATTERNED, AnimationVars.ORDERED));
        for (String option : list) menu.getItems().add(option);
        menu.setValue(a.startGrid);
        menu.setOnAction(e -> selectGrid(menu.getValue()));
    }

    private void selectGrid(String value) {
        a.startGrid = value;
        m.start();
    }

    private void setupDropdownMenuPatterns(GridPane grid) {
        final ComboBox<StartPattern> menu = new ComboBox<>();
        setComboBox(menu, grid, vPosControls++);
        EnumSet<StartPattern> set = EnumSet.allOf(StartPattern.class);
        for (StartPattern option : set) menu.getItems().add(option);
        menu.setValue(a.startPattern);
        menu.setOnAction(e -> selectStartPattern(menu.getValue()));
    }

    private void selectStartPattern(StartPattern value) {
        a.startPattern = value;
        m.start();
    }

    private <T> StringConverter<T> getStringConverter() {
        return new StringConverter<>() {
            @Override
            public String toString(T item) {
                if (item instanceof AutomatonEnum) {
                    return ((AutomatonEnum) item).getName();
                } else if (item instanceof GridDimension) {
                    return ((GridDimension) item).label;
                } else if (item instanceof StartPattern) {
                    return String.format("%s (%d times)", ((StartPattern) item).getName(), ((StartPattern) item).getAmount());
                } else return item.toString();
            }

            @Override
            public T fromString(String id) {
                return null;
            }
        };
    }

    static class GridDimension {

        private final int length;
        private final String label;

        GridDimension(String label, int length) {
            this.length = length;
            this.label = label + length;
        }
    }

    private static final String MAZE_WIDTH_LABEL = "Grid width: ", MAZE_HEIGHT_LABEL = "Grid height: ";
    private final int MIN = 5, INTERVAL = 10, SIZE = 10;

    private final ComboBox<GridDimension> mazeWidthMenu = new ComboBox<>();

    private void setupDropdownMenuGridWidth(GridPane grid) {
        setComboBox(mazeWidthMenu, grid, vPosControls++);
        List<GridDimension> gridDimensionList = getDimensionList(MAZE_WIDTH_LABEL, MIN, INTERVAL, SIZE);
        for (GridDimension d : gridDimensionList) mazeWidthMenu.getItems().add(d);
        mazeWidthMenu.setValue(new GridDimension(MAZE_WIDTH_LABEL, a.gridSize.width));
        mazeWidthMenu.setOnAction(e -> setGridWidth(mazeWidthMenu.getValue().length));
    }

    private void setGridWidth(int width) {
        m.getTimeline().stop();
        int height = a.gridSize.height;
        setCanvasSize(m.getBgCanvas(), width, height);
        a.gridSize = new Dimension(width, height);
        m.restartSim();
    }

    private final ComboBox<GridDimension> mazeHeightMenu = new ComboBox<>();

    private void setupDropdownMenuGridHeight(GridPane grid) {
        setComboBox(mazeHeightMenu, grid, vPosControls++);
        List<GridDimension> gridDimensionList = getDimensionList(MAZE_HEIGHT_LABEL, MIN, INTERVAL, SIZE);
        for (GridDimension d : gridDimensionList) mazeHeightMenu.getItems().add(d);
        mazeHeightMenu.setValue(new GridDimension(MAZE_HEIGHT_LABEL, a.gridSize.height));
        mazeHeightMenu.setOnAction(e -> setGridHeight(mazeHeightMenu.getValue().length));
    }

    private void setGridHeight(int height) {
        m.getTimeline().stop();
        int width = a.gridSize.width;
        setCanvasSize(m.getBgCanvas(), width, height);
        a.gridSize = new Dimension(width, height);
        m.restartSim();
    }

    private void setupDropdownMenuAmountOfInitAliveCells(GridPane grid) {
        final ComboBox<String> menu = new ComboBox<>();
        setComboBox(menu, grid, vPosControls++);
        for (int i = 0; i <= 40; i++) menu.getItems().add("Init Alive Cells: " + i);
        menu.setValue("Imperfect: " + a.imperfectPercentage + " %");
        menu.setOnAction(e -> a.imperfectPercentage = Integer.parseInt(menu.getValue().substring(11, menu.getValue().length() - 2)));
    }

    private <T> void setComboBox(ComboBox<T> comboBox, GridPane grid, int vPos) {
        comboBox.setPrefWidth(PREF_WIDTH_MENU);
        comboBox.setConverter(getStringConverter());
        grid.add(comboBox, 0, vPos, 2, 1);
    }

    private List<String> getSolveMethodList() {
        List<String> list = new ArrayList<>();
        list.add(GAME_OF_LIFE);
        list.add(OTHER_1);
        list.add(OTHER_2);
        return list;
    }

    private void setupColorPickers(GridPane grid) {
        setupWallColorPickers(grid);
        setupPathColorPickers(grid);
        setupSolutionColorPickers(grid);
        setupOtherColorPickers(grid);
        c.colorUpdater = () -> {
            wallColorPicker.setValue(c.offColor);
            bWallColorPicker.setValue(c.bottomoffColor);
            pathColorPicker.setValue(c.onColor);
            bPathColorPicker.setValue(c.bottomOnColor);
            beginSolveCp.setValue(c.beginSolutionColor);
            endSolveCp.setValue(c.endSolutionColor);
            nodeColorPicker.setValue(c.nodeColor);
            backGroundColorPicker.setValue(c.backGroundColor);
        };
    }

    private ColorPicker wallColorPicker, bWallColorPicker, pathColorPicker, bPathColorPicker,
            beginSolveCp, endSolveCp, nodeColorPicker, backGroundColorPicker;


    private void setupWallColorPickers(GridPane grid) {
        wallColorPicker = setupColorPicker(grid, c.offColor, 0, vPosControls);
        bWallColorPicker = setupColorPicker(grid, c.bottomoffColor, 1, vPosControls++);
        wallColorPicker.setOnAction(e -> {
            c.offColor = wallColorPicker.getValue();
            a.drawBackground(m.getCanvas(), acos(a.lg.getStartX()));
        });
        bWallColorPicker.setOnAction(e -> {
            c.bottomoffColor = bWallColorPicker.getValue();
            a.drawBackground(m.getCanvas(), acos(a.lg.getStartX()));
        });
    }

    private void setupPathColorPickers(GridPane grid) {
        pathColorPicker = setupColorPicker(grid, c.getOnColor(), 0, vPosControls);
        bPathColorPicker = setupColorPicker(grid, c.getBPathColor(), 1, vPosControls++);
        pathColorPicker.setOnAction(e -> c.onColor = (pathColorPicker.getValue()));
        bPathColorPicker.setOnAction(e -> c.bottomOnColor = (bPathColorPicker.getValue()));
    }

    private void setupSolutionColorPickers(GridPane grid) {
        beginSolveCp = setupColorPicker(grid, c.beginSolutionColor, 0, vPosControls);
        endSolveCp = setupColorPicker(grid, c.endSolutionColor, 1, vPosControls++);
        beginSolveCp.setOnAction(e -> c.beginSolutionColor = (beginSolveCp.getValue()));
        endSolveCp.setOnAction(e -> c.endSolutionColor = (endSolveCp.getValue()));
    }

    private void setupOtherColorPickers(GridPane grid) {
        nodeColorPicker = setupColorPicker(grid, c.nodeColor, 0, vPosControls);
        backGroundColorPicker = setupColorPicker(grid, c.backGroundColor, 1, vPosControls++);
        nodeColorPicker.setOnAction(e -> c.nodeColor = nodeColorPicker.getValue());
        backGroundColorPicker.setOnAction(e -> m.rootBcUpdater.update(backGroundColorPicker.getValue()));
    }

    private ColorPicker setupColorPicker(GridPane grid, Color startColor, int hPos, int vPos) {
        ColorPicker colorPicker = new ColorPicker(startColor);
        colorPicker.setPrefWidth(PREF_WIDTH_MENU / 2.);
        colorPicker.setStyle("-fx-color-label-visible: false ;");
        grid.add(colorPicker, hPos, vPos, 1, 1);
        return colorPicker;
    }

    public SliderUpdater sliderUpdater;

    private void setupSliders(GridPane grid) {
        Slider simSpeed = setupSlider(grid, new HorSliderInput(.5, 30, INIT_FRAMERATE, 0, vPosControls++, 2), PREF_WIDTH_MENU, true);
        Slider rotateCanvasSlider = setupSlider(grid, new HorSliderInput(-180, 180, m.getBgCanvas().getRotate(), 0, vPosControls++, 2), PREF_WIDTH_MENU, false);
        Slider zoomSlider = setupSlider(grid, new HorSliderInput(MIN_SCALE_FACTOR, MAX_SCALE_FACTOR, m.getBgCanvas().getScaleX(), 0, vPosControls, 2), PREF_WIDTH_MENU, false);
        Slider horPosSlider = setupSlider(grid, new HorSliderInput(-m.getScene().getWidth(), m.getScene().getWidth(), m.getBgCanvas().getTranslateX(), 2, vPosControls++, 2), SCREEN_SIZE.width, false);
        Slider verPosSlider = setupVerSlider(grid);
        simSpeed.valueProperty().addListener((oldVal, curVal, newVal) -> m.getTimeline().setRate(newVal.doubleValue() / INIT_FRAMERATE)); //fullCycleDuration = max.double
        rotateCanvasSlider.valueProperty().addListener((oldVal, curVal, newVal) -> m.getBgCanvas().setRotate(newVal.doubleValue()));
        zoomSlider.valueProperty().addListener((oldVal, curVal, newVal) -> {
            m.getBgCanvas().setScaleX(newVal.doubleValue());
            m.getBgCanvas().setScaleY(newVal.doubleValue());
            horPosSlider.setMin(horPosSlider.getMin() * newVal.doubleValue() / curVal.doubleValue());
            horPosSlider.setMax(horPosSlider.getMax() * newVal.doubleValue() / curVal.doubleValue());
            horPosSlider.setValue(-m.getBgCanvas().getTranslateX() * newVal.doubleValue() / curVal.doubleValue());
            verPosSlider.setMin(verPosSlider.getMin() * newVal.doubleValue() / curVal.doubleValue());
            verPosSlider.setMax(verPosSlider.getMax() * newVal.doubleValue() / curVal.doubleValue());
            verPosSlider.setValue(m.getBgCanvas().getTranslateY() * newVal.doubleValue() / curVal.doubleValue());
        });
        horPosSlider.valueProperty().addListener((oldVal, curVal, newVal) -> m.getBgCanvas().setTranslateX(-newVal.doubleValue()));
        verPosSlider.valueProperty().addListener((oldVal, curVal, newVal) -> m.getBgCanvas().setTranslateY(newVal.doubleValue()));
        sliderUpdater = (x, y, angle) -> {
            horPosSlider.setValue(x);
            verPosSlider.setValue(y);
            rotateCanvasSlider.setValue(angle);
        };
        m.zoomSliderUpdater = (zoom, var2, var3) -> zoomSlider.setValue(zoom);
    }

    private static class HorSliderInput {

        private final int hPos, vPos, hSpan;
        private final double minValue, maxValue, curValue;

        private HorSliderInput(double minValue, double maxValue, double curValue, int hPos, int vPos, int hSpan) {
            this.hPos = hPos;
            this.vPos = vPos;
            this.hSpan = hSpan;
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.curValue = curValue;
        }
    }

    private Slider setupSlider(GridPane grid, HorSliderInput i, int prefWidth, boolean setScale) {
        Slider slider = new Slider(i.minValue, i.maxValue, i.curValue);
        slider.setPrefWidth(prefWidth);
        slider.setShowTickLabels(setScale);
        slider.setShowTickMarks(setScale);
        slider.setMajorTickUnit(50);
        slider.setMinorTickCount(5);
        slider.setBlockIncrement(10);
        grid.add(slider, i.hPos, i.vPos, i.hSpan, 1);
        return slider;
    }

    private Slider setupVerSlider(GridPane grid) {
        Slider slider = new Slider(-m.getScene().getHeight(), m.getScene().getHeight(), m.getBgCanvas().getTranslateY());
        slider.setPrefHeight(SCREEN_SIZE.height);
        slider.setOrientation(Orientation.VERTICAL);
        grid.add(slider, 6, 0, 1, vPosControls);
        return slider;
    }

}
