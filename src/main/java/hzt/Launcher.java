package hzt;

import hzt.controller.AppManager;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

public class Launcher extends Application {


    public static String locationOfJar;

    public static void main(String[] args) {
        new Launcher().getRoot();
        try {
            launch(args);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Application will stop now");
            System.exit(0);
        }
    }

    private void getRoot() {
        try {
            URL uri = getClass().getProtectionDomain().getCodeSource().getLocation();
            File f = new File(uri.toURI());
            locationOfJar = f.getParent();
            System.out.println("This program will search for an input folder and output within the directory where the jar is located...");
            System.out.println("Location of jar: " + locationOfJar);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage stage) {
        new AppManager(stage);
    }
}
