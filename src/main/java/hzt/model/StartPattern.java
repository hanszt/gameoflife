package hzt.model;

import java.util.Arrays;

public enum StartPattern {

    GLIDER("gliders", getGliderPattern(), true, 20),
    SPACESHIP("spaceship", getSpaceshipPattern(), false, 10),
    EATER("eater", getEaterPattern(), true, 20),
    SNAKE("snake", getSnakePattern(), true, 4),
    EXPLOSION("explosion", getExplosionPattern(), false, 34),
    BLOCK("block", getBlock(), true,5),
    PULSAR("pulsar", getPulsarPattern(), true,9),
    GLIDER_GUN("Glidergun", getGliderGun(), true,1);

    private final String name;
    private final boolean[][] pattern;
    private boolean random;
    private int amount;

    private StartPattern(String name, boolean[][] pattern, boolean random, int amount) {
        this.name = name;
        this.pattern = pattern;
        this.random = random;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public boolean[][] getPattern() {
        return pattern;
    }

    public boolean isRandom() {
        return random;
    }

    public void setRandom(boolean random) {
        this.random = random;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    private static boolean[][] getGliderPattern() {
        boolean[][] gliderPattern = new boolean[3][3];
        gliderPattern[1][0] = true;
        gliderPattern[2][1] = true;
        gliderPattern[0][2] = true;
        gliderPattern[1][2] = true;
        gliderPattern[2][2] = true;
        return gliderPattern;
    }

    private static boolean[][] getSpaceshipPattern() {
        boolean[][] spaceshipPattern = new boolean[6][4];
        spaceshipPattern[1][0] = true;
        spaceshipPattern[2][0] = true;
        spaceshipPattern[3][0] = true;
        spaceshipPattern[0][1] = true;
        spaceshipPattern[1][1] = true;
        spaceshipPattern[2][1] = true;
        spaceshipPattern[3][1] = true;
        spaceshipPattern[4][1] = true;
        spaceshipPattern[0][2] = true;
        spaceshipPattern[1][2] = true;
        spaceshipPattern[2][2] = true;
        spaceshipPattern[4][2] = true;
        spaceshipPattern[5][2] = true;
        spaceshipPattern[3][3] = true;
        spaceshipPattern[4][3] = true;
        return spaceshipPattern;
    }

    private static boolean[][] getEaterPattern() {
        boolean[][] eaterPattern = new boolean[4][4];
        eaterPattern[0][0] = true;
        eaterPattern[1][0] = true;
        eaterPattern[0][1] = true;
        eaterPattern[2][1] = true;
        eaterPattern[2][2] = true;
        eaterPattern[2][3] = true;
        eaterPattern[3][3] = true;
        return eaterPattern;
    }

    private static boolean[][] getSnakePattern() {
        boolean[][] snakePattern = new boolean[8][2];
        snakePattern[0][0] = true;
        snakePattern[2][0] = true;
        snakePattern[3][0] = true;
        snakePattern[0][1] = true;
        snakePattern[1][1] = true;
        snakePattern[3][1] = true;
        return snakePattern;
    }

    private static boolean[][] getExplosionPattern() {
        return new boolean[][]{
                {false, true, false},
                {true, true, true},
                {false, false, true}};
    }

    private static boolean[][] getExplosionPattern2() {
        return new boolean[][]{
                {false, false, true, false, false},
                {false, true, false, true, false},
                {true, true, false, true, true},
                {false, true, false, true, false},
                {false, false, true, false, false}
        };
    }

    private static boolean[][] getPentaDecathlonPattern() {
        boolean[][] pentaDecathlonPattern = new boolean[3][8];
        for (int y = 0; y < pentaDecathlonPattern[0].length; y++) {
            for (int x = 0; x < pentaDecathlonPattern.length; x++) {
                pentaDecathlonPattern[x][y] = true;
            }
        }
        pentaDecathlonPattern[1][1] = false;
        pentaDecathlonPattern[1][6] = false;
        return pentaDecathlonPattern;
    }

    private static boolean[][] getPulsarPattern() {
        boolean[][] pulsarPattern = new boolean[13][13];
        for (int y = 0; y < pulsarPattern[0].length; y++) {
            for (int x = 0; x < pulsarPattern.length; x++) {
                if ((x == 0 || x == 5 || x == 7 || x == 12) && ((y > 1 && y < 5) || (y > 7 && y < 11))) {
                    pulsarPattern[x][y] = true;
                }
                if ((y == 0 || y == 5 || y == 7 || y == 12) && ((x > 1 && x < 5) || (x > 7 && x < 11))) {
                    pulsarPattern[x][y] = true;
                }
            }
        }
        return pulsarPattern;
    }

    private static boolean[][] getBlock() {
        boolean[][] blockPattern = new boolean[2][2];
        boolean[] array = {true, true};
        Arrays.fill(blockPattern, array);
        return blockPattern;
    }

    private static boolean[][] getGliderGunPart1() {
        boolean[][] part1 = new boolean[6][7];
        for (int y = 0; y < part1[0].length; y++) {
            for (int x = 0; x < part1.length; x++) {
                if ((x == 0 || x == 4 || x == 5) && ((y > 1 && y < 5))) part1[x][y] = true;
                if ((x == 1 || x == 5) && (y == 1 || y == 3)) part1[x][y] = true;
                if ((x == 2) && (y == 0 || y == 6)) part1[x][y] = true;
            }
        }
        return part1;
    }

    private static boolean[][] getGliderGunPart2() {
        boolean[][] part2 = new boolean[6][7];
        for (int y = 0; y < part2[0].length; y++) {
            for (int x = 0; x < part2.length; x++) {
                if ((x == 0 || x == 4 || x == 5) && ((y > 1 && y < 5))) part2[x][y] = true;
                if ((x == 1 || x == 5) && (y == 1 || y == 3)) part2[x][y] = true;
                if ((x == 2) && (y == 0 || y == 6)) part2[x][y] = true;
            }
        }
        return part2;
    }

    private static boolean[][] getGliderGun() {
        boolean[][] gliderGun = new boolean[100][];
        return gliderGun;
    }

    @Override
    public String toString() {
        return "StartPattern{" +
                "name='" + name + '\'' +
                ", pattern=" + Arrays.toString(pattern) +
                ", random=" + random +
                '}';
    }
}
