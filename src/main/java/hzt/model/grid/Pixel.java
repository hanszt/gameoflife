package hzt.model.grid;

public class Pixel implements Comparable<Pixel>{

    private static int nrOfPixels = 0;
    private final int nr, x, y;
    private int solutionEntryNr = -1;
    private boolean isAlive;

    public Pixel(int x, int y, boolean isAlive) {
        this.x = x;
        this.y = y;
        this.isAlive = isAlive;
        this.nr = ++nrOfPixels;
    }

    @Override
    public int compareTo(Pixel other) {
        return this.x - other.getX();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public int getSolutionEntryNr() {
        return solutionEntryNr;
    }

    public void setSolutionEntryNr(int solutionEntryNr) {
        this.solutionEntryNr = solutionEntryNr;
    }

    @Override
    public String toString() {
        return "Pixel{" +
                "pixel number=" + nr +
                ", x position=" + x +
                ", y position=" + y +
                ", solutionEntry number=" + solutionEntryNr +
                ", isAlive=" + isAlive +
                '}';
    }
}